package com.example.lukasztask.app.common;

public enum CurrencyConstants {
    USD, PLN, CHF, AFN, EUR, ALL, AUD, RON, GBP
}

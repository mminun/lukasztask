# LukaszTaskApplication #
##uses https://api.nbp.pl in order to get mid value of the chosen currency and current gold price in polish zloty.

http://localhost:8080/gold - takes no parameters, displays json with "data" and "cena" of gold unit.

http://localhost:8080/mid?currency=usd - takes currency short version as value, and currency as key. Displays effective date of the price and medium value of the price of that
day.

http://localhost:8080/swagger-ui.html - sample swagger with UI for the application.

healthcheck: it is shared responsibility: http://localhost:8080/health and NBPHealthChecker both perform health check.
Example /health response:

{
status: "UP",
components: {
diskSpace: {
status: "UP",
details: {
total: 255820034048,
free: 89954340864,
threshold: 10485760
}
},
ping: {
status: "UP"
}
}
}

NBPHealthChecker every one second tries to use both of the services. If not, Exception is thrown and printed on the screen.